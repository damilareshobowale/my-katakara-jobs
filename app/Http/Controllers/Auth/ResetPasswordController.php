<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Notifications\PasswordResetRequest;
use App\Notifications\PasswordResetSuccess;
use App\User;
use App\PasswordReset;
use Validator;


class ResetPasswordController extends Controller
{
    /**
     * Create token password reset
     *
     * @param  [string] email
     * @return [string] message
     */
    public function create(Request $request)
    {
        $login_type = filter_var($request->input('login'), FILTER_VALIDATE_EMAIL ) ? 'email' : 'name';
        $request->merge([$login_type => $request->input('login')]);
        if ($login_type == 'email') {
            $validator = Validator::make($request->all(), [
                'email' => 'required|string|email',
            ]);
        }
        else {
            $validator = Validator::make($request->all(), [
                'name' => 'required|string',
            ]);
        }

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first(), 'input' => $request->input('login')], 401);
        }

        try {
            $user = User::where('email', $request->email)->first();
            if (!$user)
                return response()->json([
                    'error' => "Sorry, we can't find the user. Please try signing up"
                ], 404);
    
            $passwordReset = PasswordReset::updateOrCreate(
                ['email' => $user->email],
                [
                    'email' => $user->email,
                    'token' => $user->createToken(env('APP_RESET_TOKEN'))->accessToken,
                 ]
            );
            if ($user && $passwordReset)
                $user->notify(
                    new PasswordResetRequest($passwordReset->token, $user->email)
                );

        } 
        catch (\Illuminate\Database\QueryException $exception) {
            return response()->json(['error' => $exception], 400);
        }
        catch (PDOException $exception) {
            return response()->json(['error' => 'Sorry, there was an error saving your information.'], 400);
        }
        catch (Swift_TransportException $exception) {
            return response()->json(['error' => 'Sorry, we were not able to connect to the mail server.'], 400);
        }
       
        return response()->json([
            'message' => 'We have e-mailed your password reset link!'
        ], 200);
    }


    /**
     * Find token password reset
     *
     * @param  [string] $token
     * @return [string] message
     * @return [json] passwordReset object
     */
    public function find($token)
    {
        try {
            $passwordReset = PasswordReset::where('token', $token)
            ->first();
            if (!$passwordReset)
                return response()->json([
                    'error' => 'This password reset token is invalid.'
                ], 404);
            if (Carbon::parse($passwordReset->updated_at)->addMinutes(720)->isPast()) {
                $passwordReset->delete();
                return response()->json([
                    'error' => 'This password reset token is invalid.'
                ], 404);
            }
        }
        catch (\Illuminate\Database\QueryException $exception) {
            return response()->json(['error' => 'An error occured'], 400);
        }
        catch (PDOException $exception) {
            return response()->json(['error' => 'Sorry, there was an error getting your token.'], 400);
        } 

        return response()->json([
            'message' => 'Token was valid', 200]);
        
    }
     /**
     * Reset password
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @param  [string] token
     * @return [string] message
     * @return [json] user object
     */
    public function reset(Request $request)
    {
        $login_type = filter_var($request->input('login'), FILTER_VALIDATE_EMAIL ) ? 'email' : 'name';
        $request->merge([$login_type => $request->input('login')]);
        if ($login_type == 'email') {
            $validator = Validator::make($request->all(), [
                'email' => 'required|string|email',
            ]);
        }
        else {
            $validator = Validator::make($request->all(), [
                'name' => 'required|string',
            ]);
        }

        $validator = Validator::make($request->all(), [
            'password' => 'required|string|confirmed',
            'token' => 'required|string'
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first(), 'input' => $request->input('login')], 401);
        }

        try {
            $passwordReset = PasswordReset::where([
                ['token', $request->token],
                ['email', $request->email]
            ])->first();
            if (!$passwordReset)
                return response()->json([
                    'message' => 'This password reset token is invalid.'
                ], 404);
            $user = User::where('email', $passwordReset->email)->first();
            if (!$user)
                return response()->json([
                    'message' => "We can't find a user with that e-mail address."
                ], 404);
            $user->password = bcrypt($request->password);
            $user->save();
            $passwordReset->delete();
        }
        catch (\Illuminate\Database\QueryException $exception) {
            return response()->json(['error' => $exception], 400);
        }
        catch (PDOException $exception) {
            return response()->json(['error' => 'Sorry, there was an error saving your information.'], 400);
        }
        
        $user->notify(new PasswordResetSuccess($passwordReset));
        return response()->json([
            'message' => 'Password reseted',
            'user' => $user,
        ], 200);
    }
}