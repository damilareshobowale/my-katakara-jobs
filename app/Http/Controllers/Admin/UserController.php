<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\User;
use Auth;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    //
     /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return response()->json(User::all(),200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|unique:users|max:50',
            'email' => 'required|unique:users|email',
            'password' => 'required|min:6',
            'c_password' => 'required|same:password',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 401);
        }

        $data = $request->only(['firstname', 'lastname', 'name', 'email', 'password']);
        $data['password'] = bcrypt($data['password']);

        try { 
            $user = User::create($data);
        }
        catch (\Illuminate\Database\QueryException $exception) {
            return response()->json(['error' => 'Email already exists'], 400);
        }
        catch (PDOException $exception) {
            return response()->json(['error' => 'Sorry, there was an error saving your information.'], 400);
        }

        return response()->json([
            'user' => $user
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Inventory  $inventory
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
        return response()->json([
            'message' => $user ? 'User infomation success' : 'Sorry, User do not exists',
            'data' => $user,
        ], $user ? 200 : 400);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Inventory  $inventory
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
        return response()->json([
            'message' => $inventory ? 'User information success' : 'Sorry, User do not exists',
            'data' => $inventory,
        ], $inventory ? 200 : 400);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Inventory  $inventory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
        //
        $validator = Validator::make($request->all(), [
            'password' => 'required|min:6',
            'c_password' => 'required|same:password',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 401);
        }

        try {
            $user->firstname = $request->firstname;
            $user->lastname = $request->lastname;
            $user->password = bcrypt($request->password);
            //handles image uploads
            if($request->image)
            {
                $image = $request->get('image');
                $name = time().'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
                \Image::make($request->get('image'))->save(public_path('images/users/').$name);
                $inventory->image_name = $name;
            }
            $inventory->save();

        }
        catch (\Illuminate\Database\QueryException $exception) {
            return response()->json([
                'error' => 'we couldnt save the user info'], 400);
        }
        catch (PDOException $exception) {
            return response()->json(['error' => 'Sorry, there was an error saving your user information.'], 400);
        }

        return response()->json([
            'data'   => $inventory,
            'message' => 'User successfully updated'
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Inventory  $inventory
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
        $status = $user->delete();

        return response()->json([
            'message' => $status ? 'User Deleted' : 'Error Deleting User'
        ], $status ? 200 : 400);
    }
}
