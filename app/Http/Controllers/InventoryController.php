<?php

namespace App\Http\Controllers;

use App\Inventory;
use Illuminate\Http\Request;
use Auth;
use Validator;

class InventoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return response()->json(Inventory::all(),200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'price' => 'required',
            'quantity' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 401);
        }

        try {
            $inventories = Inventory::create([
                'name' => $request->name,
                'price' => $request->price,
                'quantity' => $request->quantity,
                'description' => $request->description,
                'user_id' => Auth::id(),
            ]);
            //handles image uploads
            if($request->image)
            {
            $image = $request->get('image');
            $name = time().'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
            \Image::make($request->get('image'))->save(public_path('images/').$name);
            }
            $inventories->image_name = $name;
            $inventories->save();

        }
        catch (\Illuminate\Database\QueryException $exception) {
            return response()->json([
                'error' => 'there was a problem adding this inventory'], 400);
        }

        return response()->json([
            'data'   => $inventories,
            'message' => 'Inventory successfully added'
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Inventory  $inventory
     * @return \Illuminate\Http\Response
     */
    public function show(Inventory $inventory)
    {
        //
        return response()->json([
            'message' => $inventory ? 'Inventory success' : 'Sorry, Inventory do not exists',
            'data' => $inventory,
        ], $inventory ? 200 : 400);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Inventory  $inventory
     * @return \Illuminate\Http\Response
     */
    public function edit(Inventory $inventory)
    {
        //
        return response()->json([
            'message' => $inventory ? 'Inventory success' : 'Sorry, Inventory do not exists',
            'data' => $inventory,
        ], $inventory ? 200 : 400);
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Inventory  $inventory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Inventory $inventory)
    {
        //
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'price' => 'required',
            'quantity' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()->first()], 401);
        }

        try {
            $inventory->name = $request->name;
            $inventory->price = $request->price;
            $inventory->quantity = $request->quantity;
            $inventory->description = $request->description;
            $inventory->user_id = Auth::id();
            //handles image uploads
            if($request->image)
            {
                $image = $request->get('image');
                $name = time().'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
                \Image::make($request->get('image'))->save(public_path('images/').$name);
                $inventory->image_name = $name;
            }
            $inventory->save();

        }
        catch (\Illuminate\Database\QueryException $exception) {
            return response()->json([
                'error' => $exception], 400);
        }

        return response()->json([
            'data'   => $inventory,
            'message' => 'Inventory successfully updated'
        ], 200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Inventory  $inventory
     * @return \Illuminate\Http\Response
     */
    public function destroy(Inventory $inventory)
    {
        //
        $status = $inventory->delete();

        return response()->json([
            'message' => $status ? 'Inventory Deleted' : 'Error Deleting Inventory'
        ], $status ? 200 : 400);
    }
}
