import Login from './views/pages/auth/Login'
import Register from './views/pages/auth/Register'
import ResetPasswordForm from './views/pages/auth/ResetPasswordForm'
import ForgotPassword from './views/pages/auth/ForgotPassword'
import Inventory from './views/pages/inventory/Inventory'
import InventoryLists from './views/pages/inventory/InventoryLists'
import InventoryCreate from './views/pages/inventory/InventoryCreate'
import InventoryEdit from './views/pages/inventory/InventoryEdit'
import InventoryView from './views/pages/inventory/InventoryView'
import Users from './views/pages/admin/users/Users'
import UsersLists from './views/pages/admin/users/UsersLists'
import UsersCreate from './views/pages/admin/users/UsersCreate'
import UsersView from './views/pages/admin/users/UsersView'


export const routes = [
    {
        path: '/', 
        name: 'home',
        component: InventoryLists
    },
    {
        path: '/login', 
        name: 'login',
        component: Login
    },
    {
        path: '/register',
        name: 'register',
        component: Register
    },
    {
        path: '/forgot-password',
        name: 'forgot-password',
        component: ForgotPassword,
        meta: {
                requiresAuth: false,
            },
    },
    { 
        path: '/reset-password/:token', 
        name: 'reset-password-form', 
        component: ResetPasswordForm, 
        meta: { 
            requiresAuth: false,
        },
    },
    {
        path: '/profile',
        name: 'profile',
        component: UsersView,
    },
    {   
        path: '/inventory',
        name: 'inventory',
        component: Inventory, 
        children: [
            {
            path: '', 
            name: 'inventory',
            component: InventoryLists,
            },
            {
            path: 'create',
            name: 'inventory.create',
            component: InventoryCreate,
            meta: {
                    requiresAuth: true,
                },
            },
            {
            path: ':eid/edit',
            name: 'inventory.edit',
            component: InventoryEdit,
            props: (route) => ({ eid: route.query.eid }),
            meta: {
                    requiresAuth: true,
                },
            },
            {
            path: ':id',
            name: 'inventory.view',
            component: InventoryView,
            },
      ]
    },
        {   
        path: '/user',
        name: 'users',
        component: Users, 
        meta: {
            requiresAuth: true,
            is_admin: true,
        },
        children: [
            {
            path: '', 
            name: 'users',
            component: UsersLists,
            },
            {
            path: 'create',
            name: 'user.create',
            component: UsersCreate,
            },
            {
            path: ':uid/',
            name: 'user.view',
            props: (route) => ({ eid: route.query.uid }),
            component: UsersView,
            },
      ]
    },
]