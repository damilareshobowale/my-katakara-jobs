import * as types from '../types';
import toast from '../../services/toast';

const state = {
    users: [],
};

const mutations = {
    'GET_ALL_USERS' (state, data) {
        state.users = data;
        state.isLoading = false;
    },
    'SET_IS_EMPTY_FALSE' (state, data) {
        state.isEmpty = true;
    },
    'ADD_TO_USER' (state, data) {
        state.isSuccess = data;
    },
}

const actions = {
    getAllUsers({commit}) {
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + this.getters.token;
        axios.get("/api/users/").then(response => {
            const users = response.data;
            commit('GET_ALL_USERS', users);
        }).catch(error => {
            toast.error('Users lists cannot be loaded. Please check your network');
            commit('SET_IS_EMPTY_FALSE');
        });
    },
    addUser({commit}, {firstname, lastname, name, email, password, c_password, image}) {
        axios.defaults.headers.common['Content-Type'] = 'application/json';
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + this.getters.token;

        axios.post('/api/users', {firstname, lastname, name, email, password, c_password, image}).then(response => {
            commit('ADD_TO_USER', true);
            toast.success('-:) Yay!, New User has been added.');
        }).catch(error => {
            toast.error('Guess there was an error saving your user');
        });
    },
}

const getters = {
    users: state => {
        return state.users;
    }
}

export default {
    state,
    mutations,
    actions,
    getters
}
