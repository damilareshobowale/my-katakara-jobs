import Vue from 'vue';
import Vuex from 'vuex';

import auth from './modules/auth';
import inventories from './modules/inventories';
import user from './modules/user';
import header from './modules/header';

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        auth,
        inventories,
        user,
        header,
    }
})